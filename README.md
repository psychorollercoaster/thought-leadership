What Do I Need to Know about Thought Leadership?

Toyota’s earliest beginnings came from its rural, agricultural roots. The very name of the founding family Toyoda (yes, with a “D”) means “green field”. The founders in the beginning were what we in the United States might call country bumpkins. They were simple farmers who grew up in an environment of resource scarcity, but who also possessed an abundance of common sense and resourcefulness. It is from those early lessons that Toyota has emerged as the company we know today. And, ‘lean’, which is a term applied by others to describe what they observed at Toyota, is similarly based on some very basic foundational principles that we will explore in depth later. [https://thoughtleadershipzen.blogspot.com/]

So, don’t let anybody tell you that lean is complicated. It is simple, but it’s not easy. The great philosopher and humorist Will Rogers famously said about opinionated people, “It isn’t what we don’t know that gives us trouble, it’s what we know that ain’t so”.

So, my message to you is don’t let folks convince you to hire them as consultants if they have over-complicated it and made it seem like you need somebody smart to understand lean. On the other hand, do call on seasoned professionals who make their living simplifying your enterprise and have a track record of successful lean implementations.

Back to History
Here are some of the key players you should know about. The original founder of the company was a guy named Sakichi Toyoda. He grew up in a hilly area of rural Japan in what you would imagine to be a log cabin surrounded by a simply wonderful bamboo forest. You can hear the trees clacking together in the wind. What an awesome treat for the senses! It left an indelible impression on me.

In the main area of their simple home was an open space with a hibachi of sorts used primarily for warmth and cooking. In a corner, a very crude wooden loom sat idle. Not much else to see. It did help me understand the truly humble beginnings of this incredible company, however.

In the late 1800s and early 1900s, the Toyoda family was struggling to make ends meet. Japan’s economy at the time was a simple system of bartering and horse-trading like in so many other underdeveloped countries.

Sakichi’s dad was a farmer who supplemented the family income by selling fabric his wife made at home on a crude, wooden, and handmade loom. While he was growing up, Sakichi, as well as other Japanese people, became aware that Japan had fallen far behind the rest of the developed world. They were shocked and embarrassed by the advancements made outside their country during their self-imposed isolation.

When Commodore Perry from the US Navy and his frightening steamships arrived in Japanese waters, the Japanese saw how advanced the ‘outside world’ had become, and there was a collective gasp across their

country as they realized how seriously behind the rest of the world they had become.

They were behind in nearly every respect thanks to their prior isolationist mentality. A real sense of urgency and panic spread across the country.

The emperor reached out to the entire country with a single but inspiring message, paraphrased as “We need to catch up. And fast!” He asked that every citizen start to think of ways to do so. He wanted to encourage young Japanese to start using their innate gifts and talents of invention and innovation to start to close those huge gaps.

Heeding the emperor’s call, Sakichi looked around at his humble surroundings to find what, if anything, he could do to help his country. But what could a simple, rural farm boy do? All he saw was his father farming and his mother slaving away on her crude old handmade loom. “That’s it!” he realized. He decided that he could indeed make

a better loom to help her, other weavers, and who knows, maybe the country.

From my research of his early life, it seems that Sakichi was an odd child, not one who mixed easily with other kids in his age. Some body say he had a similar personality to young Thomas Edison. Apparently, he had trouble getting along with boys in his own age, who teased him about engaging in weaving, which was, after all, considered as women’s work at the time.

Nevertheless, he was able to resist the relentless peer pressure and soldiered on in his work, deriving motivation from within, hoping that he might well be doing something important for his beloved Japan.

And so, he focused his energies entirely on this pursuit, even drawing design ideas and sketches by candlelight. As time went by, not only did he succeed in making many improvements to his looms, but he also became known as the inventor of the finest looms in the entire world, even better than the best Europe had to offer. This is still true today.

Indeed, Sakichi is acknowledged today as one of the single most significant figures in Japanese history. In Japan, he is still revered as no less an icon than the legendary Thomas Edison in the United States! In fact, he is widely considered the “King of Japanese Inventors”, with over 100 patents to his name.

One of his earliest inventions—as he pursued powered, automatic looms—was a startlingly simple innovation. He realized that even a single broken strand of yarn, if not detected in the weaving process, caused defective fabric that had to be either completely scrapped or which required precious time to repair even if it was detected soon after it occurred. Later, we will dive deeply into the notion of the waste resulting from this kind of rework.

But how could he prevent the defects, he wondered? He had an idea! His simple but breakthrough idea was to apply tension to each strand of yarn with a simple weight so that, if any of the threads broke, the weight would drop and engage a device to automatically stop the machine, thus preventing defective yarn from entering the loom in the first place.

Not only did this technology stop defects at their source, but it also allowed a single operator to monitor many looms simultaneously, knowing that they would simply stop if a thread broke and lay idle until the problem was corrected. He figured that having a machine lay idle was less of a problem than that created by producing a defective product and the required time-consuming rework. As a result, he managed to accomplish not only higher quality and efficiency but also a huge reduction in rework as well.

These devices are referred to generally as poka yoke devices. They are essentially physical mechanisms for error proofing and thus allow the building in of quality at the source. It made defective cloth essentially impossible to produce! What a concept!!!

In time, Sakichi became an incredibly famous and wealthy man. He traveled the world to share his knowledge and in search of the latest ideas, opportunities and inspirations. One of those trips brought him by steamship to the United States, where he fell in love with the newfangled automobile. A flame was lit inside him!

Inspired like never before, Sakichi gave his young engineering college graduate son, Kiichiro, all the money he had saved up on one single condition that he must use it all to develop a car in Japan for the Japanese, a challenge that Kiichiro, somewhat apprehensively, accepted. And so, Toyota as we know it today was born.

So then, Kiichiro became the founder of the Toyoda car business. Suffice it that he had learned well from his father. He was smart, industrious and curious. Having grown up in his father’s textile and loom businesses, he absorbed many of the founding principles that had made them so successful.

He adapted them from the very beginning to prove that Japan could build a car as good as any in the United States. In fact, their first car, a 1936 Toyoda Model AA Sedan, was a hybrid of sorts that used a Ford drivetrain and a Chrysler Airflow body, both considered the best of the day.